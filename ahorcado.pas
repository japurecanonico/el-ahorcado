program ahorcado;
 uses crt;
 var
  aux:boolean; {si adivina o no}
  cuerpo1,cuerpo2,cuerpo3,cuerpo4,cuerpo5,cuerpo6:char; {dibujo}
  i,n,f,o,r,m,a,t,c:char; {palabra oculta}
  let:char; {usuario}
  lose,win:longint; {ahorcado/perdio}

begin
                      {valor de la palabra oculta}
 i:='_';
 n:='_';
 f:='_';
 o:='_';
 r:='_';
 m:='_';
 a:='_';
 t:='_';
 c:='_';
 cuerpo3:=' ';
 cuerpo5:=' ';
 lose:=0;
 win:=0;

 Repeat
  clrscr;
  aux:=false;
                                 {grafico}
  writeln ('                            E L   A H O R C A D O');
  writeln (' |------');
  writeln (' |     :');
  writeln (' |     ',cuerpo1);
  writeln (' |    ',cuerpo3,cuerpo2,cuerpo4);
  writeln (' |     ',cuerpo2);
  writeln (' |    ',cuerpo5,' ',cuerpo6);
  writeln (' |');
  writeln ('---');
  writeln (' ');

  writeln (i,' ',n,' ',f,' ',o,' ',r,' ',m,' ',a,' ',t,' ',i,' ',c,' ',a);

  writeln (' ');
  writeln (' escribe una letra');
  readln (let);
                                 {si adivino}
  if (let='i') and (i='_') then
   begin
    aux:=true;
    i:='i';
    win:=win+1;
   end;

  if (let='n') and (n='_') then
   begin
    aux:=true;
    n:='n';
    win:=win+1;
   end;

  if (let='f') and (f='_') then
   begin
    aux:=true;
    f:='f';
    win:=win+1;
   end;

  if (let='o') and (o='_') then
   begin
    aux:=true;
    o:='o';
    win:=win+1;
   end;

   if (let='r') and (r='_') then
   begin
    aux:=true;
    r:='r';
    win:=win+1;
   end;

   if (let='m') and (m='_') then
   begin
    aux:=true;
    m:='m';
    win:=win+1;
   end;

   if (let='a') and (a='_') then
   begin
    aux:=true;
    a:='a';
    win:=win+1;
   end;

   if (let='t') and (t='_') then
   begin
    aux:=true;
    t:='t';
    win:=win+1;
   end;

   if (let='c') and (c='_') then
   begin
    aux:=true;
    c:='c';
    win:=win+1;
   end;
                                 {no adivino}
  {pierna izquierda}
   if (aux=false) and (cuerpo5='/') then
    begin
     cuerpo6:='\';
     lose:=6;
    end;

  {pierna derecha}
   if (aux=false) and (cuerpo4='\') then
    begin
     cuerpo5:='/';
    end;

  {brazo derecho}
   if (aux=false) and (cuerpo3='/') then
    begin
     cuerpo4:='\';
    end;

  {brazo izquierdo}
   if (aux=false) and (cuerpo2='|') then
    begin
     cuerpo3:='/';
    end;

  {torso}
   if (aux=false) and (cuerpo1='O') then
    begin
     cuerpo2:='|';
    end;

  {cabeza}
   if aux=false then
    begin
     cuerpo1:='O';
    end;

  until (win=9) or (lose=6);

 If lose>=6 then
 begin
  clrscr;
                                   {grafico}
  writeln ('                            E L   A H O R C A D O');
  writeln (' |------');
  writeln (' |     :');
  writeln (' |     ',cuerpo1);
  writeln (' |    ',cuerpo3,cuerpo2,cuerpo4);
  writeln (' |     ',cuerpo2);
  writeln (' |    ',cuerpo5,' ',cuerpo6);
  writeln (' |');
  writeln ('---');
  writeln (' ');

  writeln (i,' ',n,' ',f,' ',o,' ',r,' ',m,' ',a,' ',t,' ',i,' ',c,' ',a);
  writeln (' ');
  writeln ('HAS PERDIDO');
 end;

 If win>=9 then
 begin
 clrscr;
                                  {grafico}
  writeln ('                            E L   A H O R C A D O');
  writeln (' |------');
  writeln (' |     :');
  writeln (' |     ',cuerpo1);
  writeln (' |    ',cuerpo3,cuerpo2,cuerpo4);
  writeln (' |     ',cuerpo2);
  writeln (' |    ',cuerpo5,' ',cuerpo6);
  writeln (' |');
  writeln ('---');
  writeln (' ');

  writeln (i,' ',n,' ',f,' ',o,' ',r,' ',m,' ',a,' ',t,' ',i,' ',c,' ',a);
  writeln (' ');
  writeln ('GANASTE');
 end;

 readln;
 end.